#include "readfile.h"

/**
 * @def readfile
 * read a file and and 
 * create an object of its content
 * @param FILE * file
 * @return FileContent * 
*/
FileContent * readfile(FILE * file) {
    char * single_line = (char*) malloc(sizeof(char));
    FileContent * file_content = (FileContent*) malloc(sizeof(FileContent));
    file_content->next = NULL;

    fgets(single_line, 255, file);
    file_content->data = malloc(sizeof(single_line));
    strcpy(file_content->data, single_line);

    while( fgets(single_line, 255, file) ) {
        if(strlen(single_line) != 1)
            add_file_content(file_content, single_line);
    }
    return file_content;
}

/**
 * add a line data file content object
 * @def add_file_content
 * @param FileContent * listHead
 * @param char * data
 * @return void 
*/
void add_file_content(FileContent * file_content, char * data) {
	FileContent * temp = file_content;
	while(temp->next != NULL) {
		temp = temp->next;
	}

    FileContent * next = (FileContent*) malloc(sizeof(FileContent));
    next->data = malloc(sizeof(data));
    strcpy(next->data, data);
    next->next = NULL;

    temp->next = next;
}