CC = gcc
CFLAGS = -I .
DEPS = readfile.h automate.h
OBJ = main.o automate.o create_automate.o print_automate.o readfile.o recognize.o

%.o: %c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

automate: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm *.o
