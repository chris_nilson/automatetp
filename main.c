#include "automate.h"

int main(int argc, char ** argv) {

    Automate * automate_def = create_automate_from_file("automate.def");
    print_automate(automate_def);

    char * word = (char*)malloc(sizeof(char));
    printf("Word you want to test recognition >>> ");
    scanf("%s", word);
    int is_recognized = is_recognisable(word, automate_def);

    if(is_recognized) {
        printf("The word << %s >> is recongnisable by the automate\n",  word);
    } else {
        printf("The word << %s >> cannot be recongnized by the automate\n",  word);
    }

    return EXIT_SUCCESS;
}