#include "automate.h"

/**
 * add a symbol an automate symbols
 * @def add_symbol
 * @param char value
 * @param Symbol * symbol
 * @return void 
*/
void add_symbol(char value, Symbol * symbol) {
    Symbol * temp = symbol;
    while(temp->next != NULL) {
        temp = temp->next;
    }

    Symbol * next = (Symbol*) malloc(sizeof(Symbol));
    next->value = value;
    next->next = NULL;

    temp->next = next;
}

/**
 * add a symbol an automate transitions
 * @def add_transition
 * @param char current_state
 * @param char symbol_to_read
 * @param char next_state
 * @param Automate * automate
 * @return void 
*/
void add_transition(char current_state, char symbol_to_read, 
    char next_state, Automate * automate) {
        
    Transition * temp = automate->transitions;
    while(temp->next != NULL) {
        temp = temp->next;
    }

    Transition * next = (Transition*) malloc(sizeof(Transition));
    next->current_state = current_state;
    next->symbol_to_read = symbol_to_read;
    next->next_state = next_state;
    next->next = NULL;

    temp->next = next;
}

/**
 * find and remove a character from a string
 * @def strremove
 * @param char find
 * @param char * str
 * @return void 
*/
char * strremove(char find, char * str) {
    int len = strlen(str);
    char * temp = malloc(len * sizeof(char));
    strcpy(temp, str);
    for(int i=0; i < len; i++) {
        if(temp[i] == find) {
            for(int k=i; k < len; k++) {
                temp[k] = temp[k+1];
            }
            len--;
        }
    }
    return temp;
}

/**
 * find and remove the first occurence
 * of a character from a string
 * @def strremove
 * @param char find
 * @param char * str
 * @return void 
*/

char * strremoveone(char find, char * str) {
    int len = strlen(str);
    char * temp = malloc(len * sizeof(char));
    strcpy(temp, str);
    for(int i=0; i < len; i++) {
        if(temp[i] == find) {
            for(int k=i; k < len; k++) {
                temp[k] = temp[k+1];
            }
            len--;
            break;
        }
    }
    return temp;
}

/**
 * format a read automate file by removing extra characters
 * @def format_automate
 * @param FileContent * file_content
 * @return void 
*/
void format_automate(FileContent * file_content) {
    FileContent * temp = file_content;
    while(temp != NULL) {
        temp->data = strremove('\n', temp->data);
        temp->data = strremove('[', temp->data);
        temp->data = strremove(']', temp->data);
        temp->data = strremove('(', temp->data);
        temp->data = strremove(')', temp->data);
        temp = temp->next;
    }
}