# Automate modeler from a defined file

### How to use

```sh
$ cd the_path_you_copy_the_program
$ make
$ make clean
$ ./automate
```

### How it works

The present program simulate the very basic task an automate does, that is to read instructions from a file and try to understand and represent the extracted data. The extracted data is represented as simple as possible to navigate inside. The core of the program is code in C language, so the representation the automate is done using data structures the C language offers.

The main structure we extract data from is represented as shown bellow:

```c
   /**
     * define an automate
     * @struct Automate
     * @property Symbol * symbols
     * @property Symbol * states
     * @property Symbol * initials
     * @property Symbol * finals
     * @property Transition * transitions
    */
    typedef struct Automate {
        struct Symbol * symbols;
        struct Symbol * states;
        struct Symbol * initials;
        struct Symbol * finals;
        struct Transition * transitions;
    } Automate;
```



```c
	/**
     * define a symbol for an automate and target the next
     * @struct Symbol
     * @property char * value
     * @property Symbol * next
    */
    typedef struct Symbol {
        char value;
        struct Symbol * next;
    } Symbol;
```



```c
    /**
     * define a transition for an automate and target the next
     * @struct Transition
     * @property char * current_state
     * @property char * symbol_to_read
     * @property char * next_state
     * @property Transition * next
    */
    typedef struct Transition {
        char current_state;
        char symbol_to_read;
        char next_state;
        struct Transition * next;
    } Transition;
```



Here is the accession trees for all defined  structures used inside the program

```json
Tree of type Automate
=====================
  |-+ Automate 
    |-+ symbols 
    |-+ states 
    |-+ initials 
    |-+ finales 
    |-+ Transition 
=====================
```

```json
Tree of type Symbols
====================
  |-+ Symbol 
    |-+ value 
    |-+ next 
====================
```

```json
Tree of type Transition
=======================
  |-+ Transition 
    |-+ current_state 
    |-+ next_state 
    |-+ next 
=======================
```

```json
Tree of type FileContent
========================
  |-+ FileContent 
    |-+ data 
    |-+ next 
========================
```



### Defined functions and their purpose 



```c
/**
 * read a file and and 
 * create an object of its content
 * @def readfile
 * @param FILE * file
 * @return FileContent * 
*/
FileContent * readfile(FILE * file);
```



```c
/**
 * add a line data file content object
 * @def add_file_content
 * @param FileContent * listHead
 * @param char * data
 * @return void 
*/
void add_file_content(FileContent, char * data);
```



```c
/**
 * create the struct of an automate read from a defined file path
 * @def create_automate_from_file
 * @param char * file_path: the path of the file which describe the automate
 * @return Automate * 
*/
Automate * create_automate_from_file(char * file_path);
```



```c
/**
 * print the automate in an intelligible way
 * @def print_automate
 * @param Automate * automate
 * @return void 
*/
void print_automate(Automate * automate);
```



```c
/**
 * format a read automate file by removing extra characters
 * @def format_automate
 * @param FileContent * file_content
 * @return void 
*/
void format_automate(FileContent * file_content);
```



```c
/**
 * repeat a charecter a n number time
 * @def strrepeat
 * @param char character
 * @param int n
 * @return void 
*/
void strrepeate(char character, int n);
```



```c
/**
 * add a symbol an automate transitions
 * @def add_transition
 * @param char current_state
 * @param char symbol_to_read
 * @param char next_state
 * @param Automate * automate
 * @return void 
*/
void add_transition(char current_state, char symbol_to_read, char next_state, Automate * automate);
```



```c
/**
 * add a symbol an automate symbols
 * @def add_symbol
 * @param char value
 * @param Symbol * symbol
 * @return void 
*/
void add_symbol(char value, Symbol * symbol)
```



```c
/**
 * find and remove a character from a string
 * @def strremove
 * @param char find
 * @param char * str
 * @return void 
*/
void strremove(char find, char * str);
```



```c
/**
 * find and remove the first occurence
 * of a character from a string
 * @def strremove
 * @param char find
 * @param char * str
 * @return void 
*/
char * strremoveone(char find, char * str);
```





```c

/**
 * check if or not a word can be recognized by the defined automate
 * @def is_recognisable
 * @param char * word
 * @param Automate * automate
 * @return int 
*/
int is_recognisable(char * word, Automate * automate);
```

