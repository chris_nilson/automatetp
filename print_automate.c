#include "automate.h"
#define true 1
#define false 0

/**
 * print the automate in an intelligible way
 * @def print_automate
 * @param Automate * automate
 * @return void 
*/
void print_automate(Automate * automate) {
    int n = 0;
    Symbol * temp_symbol = automate->symbols;
    printf("   ");
    while(temp_symbol != NULL) { // print symbols on one line
        printf(" | %c", temp_symbol->value);
        temp_symbol = temp_symbol->next;
        n+=4;
    }
    printf(" |");
    printf("\n");
    n+=5;
    strrepeat('-', n);

    Symbol * temp_state = automate->states;
    int can_jump;
    while(temp_state != NULL) {
        printf("| %c", temp_state->value);

        Symbol * temp_symbol = automate->symbols;
        while(temp_symbol != NULL) {
            Transition * temp_transition = automate->transitions;
            char destination;;
            can_jump = false;
            while(temp_transition != NULL) {

                if( temp_transition->current_state == temp_state->value && 
                    temp_transition->symbol_to_read == temp_symbol->value ) {
                    can_jump = true;
                    destination = temp_transition->next_state;
                    
                    // TODO for esthetic reason, we show the fist occurence
                    // TODO of the transition we found and we break there 
                    break;
                }
                temp_transition = temp_transition->next;
            }

            temp_symbol = temp_symbol->next;
            if(can_jump) {
                printf(" | %c", destination);
            } else {
                printf(" | -");
            }
        }

        printf(" |");
        printf("\n");
        strrepeat('-', n);
        temp_state = temp_state->next;
    }

    printf("\n");
}

/**
 * repeat a charecter a n number time
 * @def strrepeat
 * @param char character
 * @param int n
 * @return void 
*/
void strrepeat(char character, int n) {
    for(int i=0; i<n; i++) {
        printf("%c", character);
    }
    printf("\n");
}