#ifndef READFILE_H
#define READFILE_H

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    /**
     * @struct FileContent
     * define a line of a file and target the next line
     * @property char * data
     * @property FileContent * next
    */
    typedef struct FileContent {
        char * data; // one line of data read from a file 
        struct FileContent * next; // target to the next line
    } FileContent;

    /**
     * @def : prototypes
    */
    void add_file_content(FileContent *, char *);
    FileContent * readfile(FILE *);

#endif // !READFILE_H