#ifndef AUTOMATE_H
#define AUTOMATE_H
    #include "readfile.h"


    /**
     * define a transition for an automate and target the next
     * @struct Transition
     * @property char * current_state
     * @property char * symbol_to_read
     * @property char * next_state
     * @property Transition * next
    */
    typedef struct Transition {
        char current_state;
        char symbol_to_read;
        char next_state;
        struct Transition * next;
    } Transition;

    /**
     * define a symbol for an automate and target the next
     * @struct Symbol
     * @property char * value
     * @property Symbol * next
    */
    typedef struct Symbol {
        char value;
        struct Symbol * next;
    } Symbol;


    /**
     * define an automate
     * @struct Automate
     * @property Symbol * symbols
     * @property Symbol * states
     * @property Symbol * initials
     * @property Symbol * finals
     * @property Transition * transitions
    */
    typedef struct Automate {
        struct Symbol * symbols;
        struct Symbol * states;
        struct Symbol * initials;
        struct Symbol * finals;
        struct Transition * transitions;
    } Automate;

    /**
     * @p prototypes
    */

    char * strremove(char, char *);
    char * strremoveone(char find, char * str);
    void add_symbol(char, Symbol *);
    void add_transition(char, char, char, Automate *);
    void strrepeat(char, int );
    void format_automate(FileContent *);
    Automate * create_automate_from_file(char *);
    void print_automate(Automate *);
    int is_recognisable(char *, Automate * automate);
    void toStr(Symbol * s);


#endif // !AUTOMATE_H