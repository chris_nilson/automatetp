#include "automate.h"

/**
 * create the struct of an automate read from a defined file path
 * @def create_automate_from_file
 * @param char * file_path : the path of the file which describe the automate
 * @return Automate * 
*/
Automate * create_automate_from_file(char * file_path) {
    FILE * file = fopen(file_path, "r"); // oopen the defined file
    if(!file) { // cam we opwn the file
        printf("[Error] an error occurs opening the file at <<%s>>. \
        Make sure the files exists and that you have read rights on it.\n", file_path);
        exit(EXIT_FAILURE);
    }
    /**
     * Extraction
     * create an object of the file lines
    */
    FileContent * file_content = readfile(file);
    fclose(file); /// we close the stream of the file to free the memory

    /**
     * Transformation
    */
    format_automate(file_content);
    int transform_step = 0; char * delimiter = ",";

    // initialize the automate
    Automate * automate = (Automate*) malloc(sizeof(Automate));

    FileContent * temp = file_content;
    while(temp != NULL) {
        switch (transform_step)
        {
            case 0: { // define the automate symbols
                automate->symbols = (Symbol*) malloc(sizeof(Symbol));
                automate->symbols->next = NULL;
                
                char * token = strtok(temp->data, delimiter);
                automate->symbols->value =  token[0];
                while( (token = strtok(NULL, delimiter)) )
                    add_symbol(token[0], automate->symbols);     
                break;
            }
            case 1: { // define the automate states
                automate->states = (Symbol*) malloc(sizeof(Symbol));
                automate->states->next = NULL;
                char * token = strtok(temp->data, delimiter);

                automate->states->value =  token[0];
                while( (token = strtok(NULL, delimiter)) )
                    add_symbol(token[0], automate->states);
                break;
            }
            case 2: { // define the automate initials
                automate->initials = (Symbol*) malloc(sizeof(Symbol));
                automate->initials->next = NULL;
                char * token = strtok(temp->data, delimiter);
    
                automate->initials->value =  token[0];
                while( (token = strtok(NULL, delimiter)) )
                    add_symbol(token[0], automate->initials);
                break;
            }
            case 3: { // define the automate finals
                automate->finals = (Symbol*) malloc(sizeof(Symbol));
                automate->finals->next = NULL;
                char * token = strtok(temp->data, delimiter);

                automate->finals->value = token[0];
                while( (token = strtok(NULL, delimiter)) )
                    add_symbol(token[0], automate->finals);
                break;
            }
            /**
             * initiate automate transitions : 
             * we need to if we wanna not have the fist value NULL
            */
            case 4: {
                automate->transitions = (Transition*) malloc( sizeof(Transition));
                automate->transitions->next = NULL;

                char current_state;
                char symbol_to_read;
                char next_state;

                current_state = strtok(temp->data, delimiter)[0];
                symbol_to_read = strtok(NULL, delimiter)[0];
                next_state = strtok(NULL, delimiter)[0];

                automate->transitions->current_state = current_state;
                automate->transitions->symbol_to_read = symbol_to_read;
                // printf("SYMBOL: %c\n", automate->transitions->symbol_to_read);
                automate->transitions->next_state = next_state;
                break;
            }
             // read the rest of the transitions 
             // and add them to the automate transitions
            default: {
                char current_state;
                char symbol_to_read;
                char next_state;

                current_state = strtok(temp->data, delimiter)[0];
                symbol_to_read = strtok(NULL, delimiter)[0];
                next_state = strtok(NULL, delimiter)[0];

                add_transition(current_state, symbol_to_read, next_state, automate);
                break;
            }
        }
        transform_step++; 
        temp = temp->next;
    }
    return automate;
}