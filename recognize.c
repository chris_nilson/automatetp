#include "automate.h"

/**
 * check if or not a word can be recognized by the defined automate
 * @def is_recognisable
 * @param char * word: the word we want to recongnize
 * @param Automate * automate: the automate we want to recongnize word from
 * @return int 
*/
int is_recognisable(char * word, Automate * automate) {
    char * word_copy = word;
    char symbol_to_recognize;
    int can_recognize = 1;
    Transition * temp_transition;

    long int len  = strlen(word_copy);
    for(int i=0; i<len && can_recognize; i++) {
        symbol_to_recognize = word[i];

        can_recognize = 0;
        temp_transition = automate->transitions;
        while(temp_transition != NULL) {
            if(temp_transition->symbol_to_read == symbol_to_recognize) {
                can_recognize = 1;
                break;
            }
            temp_transition = temp_transition->next;
        }

        word_copy = strremoveone(symbol_to_recognize, word_copy);
    }

    return can_recognize;
}